﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mover : MonoBehaviour {
    public float speed;
   
    // Use this for initialization
    void Start () {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(1f, 0.0f, 1f);
        rb.velocity = transform.forward * speed;
    }
	
	
}

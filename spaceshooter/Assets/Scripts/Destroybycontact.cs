﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroybycontact : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private  GameController gameController;

    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null )
        {
            Debug.Log("게임컨트롤러 못찾음");
        }
    } 



    // Use this for initialization
    void OnTriggerEnter(Collider other)
    {
        if  (other.tag == "Boundary")
        { return;
        }
      
        Instantiate(explosion, transform.position, transform.rotation);
        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }
        
        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}